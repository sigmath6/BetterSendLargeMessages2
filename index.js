module.exports = (Plugin, Library) => {

    const {
      Logger, Patcher, Modals, DiscordClassModules, DiscordSelectors,
      DiscordModules, DOMTools, ReactTools, PluginUtilities, Toasts,
      WebpackModules, EmulatedTooltip
    } = Library;

    const ClassModules = {
      "Scrollbar": WebpackModules.getByProps('scrollbar'),
      "SizeLarge": WebpackModules.getByProps('sizeLarge'),
      "Horizontal": WebpackModules.getByProps('horizontal'),
      "InnerNoAutocomplete": WebpackModules.getByProps('innerNoAutocomplete')
    };

    const SEND_MESSAGE_HTML = require("modal.html");
    const CUSTOM_CSS = require("plugin.css");
    const MESSAGE_NOTE_SVG = require("note.svg");
    const MESSAGE_NOTELOCKED_SVG = require("notelocked.svg");
    const MESSAGE_NOTE_HTML = require("note.html");

    const MODAL_SIZE = Modals.ModalSizes.LARGE;

    const CHAR_LIMIT = 1900;
    const TOO_MANY_MESSAGES = 20;
    const MESSAGE_DELAY = 1000;
    const TIMEOUT = 200;
    const SHORT_TIMEOUT = 50;
    const WARNING_DISPLAY_TIME = 5000;
    const SAVE_PERIOD = 2500;

    const PARA_SEP = '\n';
    const SENTENCE_SEP = ' ';
    const SENTENCE_SEP_REGEX = new RegExp("(?<=[\.!?] *)"+SENTENCE_SEP, 'g');
    const WORD_SEP = ' ';

    const ENTER = 13;
    const ESC = 27;

    const KEY_CODE_ALT = 18;
    const KEY_CODE_CTRL = 17;
    const KEY_CODE_SHIFT = 16;
    const DISCORD_ALT = 164;
    const DISCORD_CTRL = 162;
    const DISCORD_SHIFT = 160;

    const ALWAYS = "always";
    const NEVER = "never";
    const FOR_SHORT_MESSAGES = "short";

    const CATEGORY = "category";
    const DROPDOWN = "dropdown";
    const VISIBLE = "visible";
    const HIDDEN = "hidden";
    const UNSET = "unset";

    const CHARCOUNTER_SELECTOR = '#charcounter';
    const MESSAGE_NOTE_SELECTOR = '.message-note';
    const TEXTAREA_TAG = "TEXTAREA";

    const DEFAULT_RIGHT_OFFSET = "0px";
    const CHARCOUNTER_RIGHT_PADDING = 5;

    const DEFAULT_SAVED_TEXT_STATE = {
      "savedText": "",
      "savedTextSelectionStart": 0,
      "savedTextSelectionEnd": 0,
      "savedTextScrollTop": 0,
      "messageLocked": false
    };

    return class BetterSendLargeMessages2 extends Plugin {
        constructor() {
          super();

          // State varaibles about chatbox and message box state
          this.savedText = "";
          this.savedTextSelectionStart = 0;
          this.savedTextSelectionEnd = 0;
          this.savedTextScrollTop = 0;
          this.savedChatText = "";
          this.savedChatSelectStart = 0;
          this.savedChatSelectEnd = 0;
          this.savedChatScrollTop = 0;
          this.deletedMessage = "";

          this.sending = false;
          this.keys = {};

          this.disableDefaultWarnings = false;
          this.savingPeriodically = false;

          this.messageLocked = false;
          this.messageLockedWarningShowed = false;
          this.messageLockedWarningShowing = false;
          this.deleteWarningToastShowing = false;

          this.rightOffset = DEFAULT_RIGHT_OFFSET;

          this.currentLocale = DiscordModules.LocaleManager.getLocale();

          // Hack because JS is shit
          this.onKeyDown = this.onKeyDown.bind(this);
          this.onKeyUp = this.onKeyUp.bind(this);
        }

        getDescription() {
          /*
            Update straggling locale strings (including description) when
            getting description
           */
          this.setRemainingLocale(DiscordModules.LocaleManager.getLocale());
          return super.getDescription();
        }

        onStart() {
            Logger.log("Started");

            PluginUtilities.addStyle(this.getName(), CUSTOM_CSS);

            this.loadMessageData();
            this.checkNote();

            this.setRemainingLocale(DiscordModules.LocaleManager.getLocale(), true);

            document.addEventListener("keydown", this.onKeyDown);
            document.addEventListener("keyup", this.onKeyUp);
        }

        onStop() {
            Logger.log("Stopped");
            Patcher.unpatchAll();
            PluginUtilities.removeStyle(this.getName());

            this.removeNoteButton();

            this.saveMessageData();

            this.savedText = "";
            this.savedChatText = "";
            this.deletedMessage = "";

            this.savingPeriodically = false;

            document.removeEventListener("keydown", this.onKeyDown);
            document.removeEventListener("keyup", this.onKeyUp);
        }

        getSettingsPanel() {
          const panel = this.buildSettingsPanel();
          let oldOnChange = panel.onChange;
          panel.onChange = () => {
            this.updateNotePosition();
            oldOnChange();
          };
          return panel.getElement();
        }

        observer(e) {
          if (!e.addedNodes.length || !e.type == "childList") return;
          if (e.target.tagName == TEXTAREA_TAG && e.target.placeholder != "") {
              let text = e.target.value;

              if (text.length > CHAR_LIMIT) {
                if (!this.messageLocked) {
                  this.showSendMessageModal(text);
                  this.setMessage("");
                } else if (!this.messageLockedWarningShowed && !this.messageLockedWarningShowing) {
                  Toasts.warning(this.strings.toast_locked_message_warning, {timeout: WARNING_DISPLAY_TIME});
                  this.messageLockedWarningShowing = true;
                  setTimeout(() => {this.messageLockedWarningShowing = false;}, WARNING_DISPLAY_TIME);
                }
                if (this.messageLockedWarningShowing) {
                  this.messageLockedWarningShowed = true;
                }
              } else {
                this.messageLockedWarningShowed = false;
              }
          }

          if (e.addedNodes[0] instanceof Element && e.addedNodes[0].querySelector(DiscordSelectors.Textarea.textArea)) {
            this.checkNote();
          }
        }

        // ------ Custom Functions ------
        showSendMessageModal(text, showWarning="") {
          /* Opens the send message modal, pre-filled with contents of text.
            Will always display text in the textarea unless it has a more up-to-date
            (longer) version saved, in which case it will display that instead
            (notifying the user).
           */
          let sendMessageModalElement = DOMTools.createElement(SEND_MESSAGE_HTML);
          let sendMessageModalReact = this.renderReactElements(sendMessageModalElement);

          if (this.savedText !== "" && this.subCompare(text, this.savedText) && this.savedText !== text) {
            showWarning = this.strings.modal_load_warning;
          } else if (text !== "") {
            if (this.savedText !== "" && !this.subCompare(this.savedText, text)) {
              showWarning = this.strings.modal_overwrite_warning;
            }
            if (this.settings.main_settings.lock_by_default && this.savedText !== text) {
              this.removeNoteButton(); //removed to trigger update/re-add later locked
              this.messageLocked = true;
            }
            this.savedText = text;
          }

          Modals.showModal(this.strings.modal_header_text, sendMessageModalReact, {
            confirmText: this.strings.btn_send_text,
            cancelText: this.strings.btn_cancel_text,
            onConfirm: () => {
              let messages = this.splitMessage(inputtext.value);

              this.sending = true;
              this.savedText = "";

              this.clearMessageData();
              this.checkNote();

              // Send all the messages
              messages.forEach((message,i) => {
                setTimeout(() => {
                  this.sendMessage(message);
                  if (i >= messages.length-1) {
                    Toasts.success(this.strings.toast_allsent_text);
                    this.sending = false;

                    if (this.savedChatText !== "") {
                      setTimeout(() => {
                        let textarea = this.chatTextbox();

                        this.restoreChatboxState(textarea);

                        if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_restored_message_info);
                      }, TIMEOUT);
                    }
                  }
                }, MESSAGE_DELAY * i);
              });
            },
            onCancel: () => {
              // If message is to be restored to chat box (not locked, no stored
              // chatbox message, no settings preventing it), just save over chatbox
              // state information with the inputtext (send large message box) Started
              // information then reload ('restore') the chatbox state
              if (
                !this.messageLocked && this.savedChatText === "" &&
                (this.settings.main_settings.restore_message === ALWAYS ||
                (this.settings.main_settings.restore_message === FOR_SHORT_MESSAGES && this.savedText.length <= CHAR_LIMIT))
              ) {
                let inputtext = this.sendMessageInputText();
                let scroll = this.savedChatScrollTop;

                this.saveChatboxState(inputtext);
                this.savedChatText = this.savedChatText.slice(0, CHAR_LIMIT);
                this.savedChatScrollTop = scroll;
                if (this.savedText === this.savedChatText) {
                  this.savedText = "";
                  this.clearMessageData();
                }
              }

              this.restoreChatboxState(chatbox);
              this.saveMessageData();
              this.checkNote();
            },
            size: MODAL_SIZE
          });

          // After modal pop up has loaded
          var inputtext = this.sendMessageInputText();
          var warning = DOMTools.Q('.bslm-modal #warning-message');
          var counter = DOMTools.Q('.bslm-modal #character-counter');
          let message_lock = DOMTools.Q('.bslm-lock #lock');

          let chatbox = this.chatTextbox();
          inputtext.placeholder = chatbox.placeholder;

          if (this.messageLocked) message_lock.style.visibility = "visible";
          else message_lock.style.visibility = "hidden";

          if (showWarning != "") {
            warning.innerText = showWarning;
            this.disableDefaultWarnings = true;
            setTimeout(() => {
              warning.innerText = "";
              this.disableDefaultWarnings = false;
            }, WARNING_DISPLAY_TIME);
          }

          // Submit and cancel shortcuts
          inputtext.addEventListener("keydown", (e) => {
      			if (e.ctrlKey && e.keyCode == ENTER) {
  				    DOMTools.Q('button[type=submit]').click();
      			}
    		  });
          inputtext.addEventListener("keydown", (e) => {
            if (e.keyCode == ESC) {
              inputtext.parents('form')[0].querySelector('button[type=button]').click();
            }
          });

          let updateCounter = () => {
            if (!this.sendMessageInputText()) return;

      			let length = inputtext.value.length;
      			let messageAmount = this.splitMessage(inputtext.value).length;
      			if (!this.disableDefaultWarnings) {warning.innerText = messageAmount > TOO_MANY_MESSAGES ? this.strings.modal_messages_warning : "";}
      			counter.innerText = length + " (" + (inputtext.selectionEnd - inputtext.selectionStart) + ") → " + this.strings.modal_messages_translation + ": " + messageAmount;

            if (
              this.settings.main_settings.lock_by_default &&
              this.savedText === "" &&
              inputtext.value !== ""
            ) {
              this.messageLocked = true;
            }

            this.saveMessageState(inputtext);

            this.checkNote();
            this.checkMessageLock();
          };

      		inputtext.addEventListener("keyup", (e) => {if (!(e.ctrlKey && e.keyCode == ENTER)) setTimeout(() => {updateCounter();}, SHORT_TIMEOUT);});
          inputtext.addEventListener("paste", () => {updateCounter();});
          inputtext.addEventListener("click", () => {updateCounter();});
          inputtext.addEventListener("scroll", () => {this.savedTextScrollTop = inputtext.scrollTop;});
          // Mouse click and drag detection logic borrowed from DevilBro
      		inputtext.addEventListener("mousedown", () => {
      			var mouseup = () => {
      				document.removeEventListener("mouseup", mouseup);
      				document.removeEventListener("mousemove", mousemove);
      			};
      			var mousemove = () => {
      				setTimeout(() => {updateCounter();}, SHORT_TIMEOUT);
      			};
      			document.addEventListener("mouseup", mouseup);
      			document.addEventListener("mousemove", mousemove);
      		});

          this.saveMessageData();
          if (!this.savingPeriodically) {
            this.savingPeriodically = true;
            var periodicSave = setInterval(() => {
              this.saveMessageData();
              if (!this.sendMessageInputText()) {
                this.savingPeriodically = false;
                clearInterval(periodicSave);
              }
            }, SAVE_PERIOD);
          }

          if (this.savedChatText === "" && chatbox.value !== "") {
            this.savedTextSelectionStart = chatbox.selectionStart;
            this.savedTextSelectionEnd = chatbox.selectionEnd;
          }

          this.restoreMessageState(inputtext);
          updateCounter();
        }

        sendMessage(text) {
          /*
            Send a single message to the currently open channel using the chatbox
           */
          let textarea = this.chatTextbox();
          if (textarea) {
            let press = new KeyboardEvent("keypress", {key: "Enter", code: "Enter", which: ENTER, keyCode: ENTER, bubbles: true});
            Object.defineProperties(press, {keyCode: {value: ENTER}, which: {value: ENTER}});

            this.setMessage(text);
            textarea.dispatchEvent(press);
          }
        }

        splitMessage(original_text) {
          /*
            Splits a large message up into several smaller messages to send individually.
            Message splitting is avoided in the middle of paragraphs or sentences
            if possible, but will default to splitting by words or characters
            if needed (on edge cases).
           */
          var messages = [];
          var text = original_text;

          // Split off next chunk as long as there is text to split
          while (text != "") {
            var vals = this.next_chunk(text.trim()); var chunk = vals[0]; text = vals[1];
            messages.push(chunk);
          }

          return messages;
        }

        saveChatboxState(chatTextbox) {
          /*
            Saves state information for the chatbox about `chatTextbox`
           */
          this.savedChatText = chatTextbox.value;
          this.savedChatSelectStart = chatTextbox.selectionStart;
          this.savedChatSelectEnd = chatTextbox.selectionEnd;
          this.savedChatScrollTop = chatTextbox.scrollTop;
        }

        restoreChatboxState(chatTextbox) {
          /*
            Restores state information for the chatbox about `chatTextbox`
           */
          this.setMessage(this.savedChatText);
          this.savedChatText = "";
          setTimeout(() => {
            chatTextbox.click();
            chatTextbox.focus();
            chatTextbox.selectionStart = this.savedChatSelectStart;
            chatTextbox.selectionEnd = this.savedChatSelectEnd;
            chatTextbox.scrollTop = this.savedChatScrollTop;
          }, TIMEOUT);
        }

        saveMessageState(inputtext) {
          /*
            Saves state information for the message box about `inputtext`
           */
          this.savedText = inputtext.value;
          this.savedTextSelectionStart = inputtext.selectionStart;
          this.savedTextSelectionEnd = inputtext.selectionEnd;
          this.savedTextScrollTop = inputtext.scrollTop;
        }

        restoreMessageState(inputtext) {
          /*
            Restores state information for the message box about `inputtext`
           */
          inputtext.value = this.savedText;
          inputtext.focus();
          inputtext.selectionStart = this.savedTextSelectionStart;
          inputtext.selectionEnd = this.savedTextSelectionEnd;
          inputtext.scrollTop = this.savedTextScrollTop;
        }

        checkMessageLock() {
          /*
            Check if the lock on the send large message dialogue should be showing
           */
          let message_lock = DOMTools.Q('.bslm-lock #lock');
          if (message_lock) {
            message_lock.style.visibility = this.messageLocked ? VISIBLE : HIDDEN;
          }
        }

        updateNotePosition() {
          /*
            Updates where the note should be positioned, taking into account
            the size of the character counter if the CharCounter plugin is
            detected.
           */
          let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
          let messageNote = DOMTools.Q(MESSAGE_NOTE_SELECTOR);
          this.rightOffset =  charcounter ? charcounter.width() + CHARCOUNTER_RIGHT_PADDING + "px" : DEFAULT_RIGHT_OFFSET;
          if (messageNote) messageNote.style.right = this.settings.main_settings.note_on_right ? this.rightOffset : UNSET;
        }

        checkNote() {
          /*
            Check if the note icon needs to be added or removed
           */
          if (this.savedText === "") {
            this.removeNoteButton();
          } else {
            this.addNoteButton();
          }

        }

        addNoteButton() {
          /*
            Add the note button icon below the chatbox.
           */
          let messageNote = DOMTools.Q('.message-note');
          if (messageNote) {
            let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
            this.updateNotePosition();
            return;
          }

          let note_svg = DOMTools.createElement(MESSAGE_NOTE_SVG)[0];
          let notelocked_svg = DOMTools.createElement(MESSAGE_NOTELOCKED_SVG)[0];
          let note = DOMTools.createElement(MESSAGE_NOTE_HTML)[0];
          if (this.settings.main_settings.note_on_right) note.style.right = this.rightOffset;
          let message_lock = DOMTools.Q('.bslm-lock #lock');
          let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
          let note_tooltip_text = "";

          if (this.messageLocked) {
            note.find('.note-svg').appendChild(notelocked_svg);
            note_tooltip_text = this.strings.note_tooltip_locked;
          } else {
            note.find('.note-svg').appendChild(note_svg);
            note_tooltip_text = this.strings.note_tooltip;
          }

          if (charcounter) {
            charcounter.addEventListener("DOMSubtreeModified", this.updateNotePosition.bind(this));
          }

          let note_tooltip = new EmulatedTooltip(note, note_tooltip_text);

          note.find('#note-button').addEventListener("click", (e) => {
            if (e.ctrlKey) {
              let message_lock = DOMTools.Q('.bslm-lock #lock');
              if (this.messageLocked) {
                this.messageLocked = false;
                note.find('.note-svg').removeChild(note.find('.note-svg').children[0]);
                note.find('.note-svg').appendChild(note_svg);
                note_tooltip.label = this.strings.note_tooltip;
                if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_message_unlocked_info);
              } else {
                this.messageLocked = true;
                note.find('.note-svg').removeChild(note.find('.note-svg').children[0]);
                note.find('.note-svg').appendChild(notelocked_svg);
                note_tooltip.label = this.strings.note_tooltip_locked;
                if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_message_locked_info);
              }
              this.saveMessageData();
              this.checkMessageLock();
            } else {
              this.saveChatboxState(this.chatTextbox());
              this.showSendMessageModal(this.savedText);
            }
          });

          DOMTools.Q(DiscordSelectors.Textarea.inner).appendChild(note);
          this.updateNotePosition();
        }

        removeNoteButton() {
          /*
            Remove the note button icon.
           */
          let note = DOMTools.Q('.message-note');
          let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
          if (note) note.parentNode.removeChild(note);
          if (charcounter) {
            charcounter.removeEventListener("DOMSubtreeModified", this.updateNotePosition.bind(this));
          }
          this.messageLocked = false;
        }

        shortcutPressed(shortcut) {
          /*
            Check if the key combination specified `shortcut` is currently being
            held down
           */
          for (let key of shortcut) {
            if (!this.keys[key]) {
              return false;
            }
          }

          for (let key in this.keys) {
            if (this.keys[key] && !shortcut.includes(parseInt(key))) {
              return false;
            }
          }

          return true;
        }

        onKeyDown(e) {
          /*
            onKeyDown event for the plugin to check when a set keyboard shortcut
            is being pressed.
           */
          if (!e.repeat) {
            this.setKey(e.keyCode, true);

            if (this.shortcutPressed(this.settings.shortcuts.open_modal_shortcut)) {
              this.showSendMessageModalShortcut();
              return;
            }

            if (this.shortcutPressed(this.settings.shortcuts.lock_message_shortcut)) {
              this.toggleMessageLockShortcut();
              return;
            }

            if (this.shortcutPressed(this.settings.shortcuts.clear_message_shortcut)) {
              this.clearMessageShortcut();
              return;
            }
          }
        }

        onKeyUp(e) {
          /*
            onKeyUp event for unsetting pressed keys.
           */
          this.setKey(e.keyCode, false);
        }

        clearMessageShortcut() {
          /*
            Executed when the clear message shortcut is pressed.
            Clears the message from the dialogue, if open, and delete saved message.
          */
          let inputtext = this.sendMessageInputText();
          if (this.savedText === "") {
            if (this.deletedMessage === "") return;
            this.savedText = this.deletedMessage;

            if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_undeleted_message_info);
            if (inputtext) {
              inputtext.value = this.deletedMessage;
              inputtext.click();
            }
            this.deletedMessage = "";
            this.saveMessageData()
          } else if (!this.messageLocked) {
            this.deletedMessage = this.savedText;
            this.savedText = "";
            if (inputtext) {
              if (!this.settings.main_settings.reduced_toasts) Toasts.error(this.strings.toast_cleared_message_warning);
              inputtext.value = "";
              inputtext.click();
            } else {
              if (!this.settings.main_settings.reduced_toasts) Toasts.error(this.strings.toast_deleted_message_warning);
            }
            this.clearMessageData();
          } else {
            if (!this.deleteWarningToastShowing) {
              this.deleteWarningToastShowing = true;
              Toasts.warning(this.strings.toast_cannot_delete_warning, {timeout: WARNING_DISPLAY_TIME});
              setTimeout(() => {
                this.deleteWarningToastShowing = false;
              }, WARNING_DISPLAY_TIME);
            }
          }
          this.checkNote();
        }

        toggleMessageLockShortcut() {
          /*
            Execute when the toggle message lock shortcut is pressed.
            Toggles if the saved message is locked, meaning it cannot be overwritten
            or deleted.
           */
          let note = DOMTools.Q('.message-note');
          if (!note) return;

          let ctrl_click_event = new MouseEvent("click", {ctrlKey: true});
          note.find('#note-button').dispatchEvent(ctrl_click_event);
        }

        showSendMessageModalShortcut() {
          /*
            Executed when the show send message shortcut is pressed.
            Opens the "Send Large Message" dialogue box.
           */

          if (!this.sending && !DOMTools.Q('div[id="user-settings"]')) {
            let chatbox = this.chatTextbox();
            if (!chatbox) return;

            let inputtext = this.sendMessageInputText();
            if (!inputtext) {
              let text = chatbox.value;

              // Don't overwrite existing saved text if opening with shortcut
              if (this.savedText === "" || this.subCompare(this.savedText, text)) {
                this.savedChatScrollTop = chatbox.scrollTop;
                this.showSendMessageModal(text);
                this.setMessage("");
              } else {
                let warning = "";
                let clear_message = false;
                this.saveChatboxState(chatbox);
                if (this.subCompare(text, this.savedText)) { // `this.savedText` includes `text`
                  clear_message = true;
                  this.savedChatText = "";
                  warning = text === "" ? "" : this.strings.modal_load_warning;
                }
                this.showSendMessageModal(this.savedText, warning);
                if (clear_message) this.setMessage("");
              }
            } else {
              inputtext.parents('form')[0].querySelector('button[type=button]').click();
            }
          }
        }

        saveMessageData() {
          /*
            Saves all information about the saved message
           */
          let name = this.getName();
          let savedTextState = {
            "savedText": this.savedText,
            "savedTextSelectionStart": this.savedTextSelectionStart,
            "savedTextSelectionEnd": this.savedTextSelectionEnd,
            "savedTextScrollTop": this.savedTextScrollTop,
            "messageLocked": this.messageLocked
          };

          PluginUtilities.saveData(name, "savedTextState", savedTextState);
        }

        loadMessageData() {
          /*
            Loades existing saved message data, if it exists
           */
          let name = this.getName();

          let loadedSavedTextState = PluginUtilities.loadData(name, "savedTextState", DEFAULT_SAVED_TEXT_STATE);
          this.savedText = loadedSavedTextState['savedText'];
          this.savedTextSelectionStart = loadedSavedTextState['savedTextSelectionStart'];
          this.savedTextSelectionEnd = loadedSavedTextState['savedTextSelectionEnd'];
          this.savedTextScrollTop = loadedSavedTextState['savedTextScrollTop'];
          this.messageLocked = loadedSavedTextState['messageLocked'];
        }

        clearMessageData() {
          /*
            Clears the saved message data
           */
          let name = this.getName();
          PluginUtilities.saveData(name, "savedTextState", DEFAULT_SAVED_TEXT_STATE);
        }

        // ------ Custom Help Functions ------
        trim_by_chunks(chunks, separator, character_limit) {
          /*
            Trims off the largest number of chunks in `chunks` possible while
            keeping under `character_limit` when the chunks are concatinated
            together separated by serparator `separator`.
            */
          var prev = "";
          var curr = "";

          for (let i = 0; i < chunks.length; i++) {
            curr += chunks[i];

            if (curr.length > character_limit) {
              break;
            }

            prev = curr;
            curr += separator;
          }

          return [prev, chunks[0]];
        }

        next_chunk(full_text) {
          /*
            Gets the next largest possible chunks by largest chunk type from
            `full_text`.
            Finding the "largest chunk type" is considered as splitting by
            paragraph, then sentence, then word, then character.
            The first chunk type tested to have any number of chunks that are
            able to be included are group together and considered the "next chunk".
           */
          var text = full_text;
          var chunk = "";

          // Try splitting paragraph
          var paragraphs = text.split(PARA_SEP);
          var vals = this.trim_by_chunks(paragraphs, PARA_SEP, CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          if (chunk != "") {
            return [chunk, full_text.replace(chunk, "")];
          }

          // Try splitting by sentence
          var sentences = text.split(SENTENCE_SEP_REGEX);
          var vals = this.trim_by_chunks(sentences, SENTENCE_SEP, CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          if (chunk != "") {
            return [chunk, full_text.replace(chunk, "")];
          }

          // Try splitting by word
          var words = text.split(WORD_SEP);
          var vals = this.trim_by_chunks(words, WORD_SEP, CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          if (chunk != "") {
            return [chunk, full_text.replace(chunk, "")];
          }

          // Try split by character
          var vals = this.trim_by_chunks(text, "", CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          return [chunk, full_text.replace(chunk, "")];
        }

        chatTextbox() {
          /*
            Returns the chatbox's textarea DOM element.
           */
          return DOMTools.Q('form').querySelector(DiscordSelectors.Textarea.textArea);
        }

        sendMessageInputText() {
          return DOMTools.Q('.bslm-modal #modal-inputtext');
        }

        subCompare(string1, string2) {
          /*
            Compares if string1 is a substring of string2
          */
          let str1 = string1;
          let str2 = string2;

          let char_limit = str1.length;

          str1 = str1.slice(0, char_limit);
          str2 = str2.slice(0, char_limit);

          return str1 == str2;
        }

        setKey(key, set) {
          /*
            Sets a key as being held down (`set` = true) or not held down
            (`set` = false) by key code, ensuring the Disord key codes are used
            for control characters.
           */
          switch (key) {
            case KEY_CODE_ALT:
              this.keys[DISCORD_ALT] = set;
              break;
            case KEY_CODE_CTRL:
              this.keys[DISCORD_CTRL] = set;
              break;
            case KEY_CODE_SHIFT:
              this.keys[DISCORD_SHIFT] = set;
              break;
            default:
              this.keys[key] = set;
              break;
          }
        }

        setMessage(text) {
          /*
            Set the message that is shown in the chatbox textarea.
           */
          let textarea = this.chatTextbox();
          let form = textarea.parents('form')[0];
          let formOwnerInstance = ReactTools.getOwnerInstance(form);
          formOwnerInstance.setState({textValue:text});
        }

        renderReactElements(elements) {
          /*
            Converts a list of elements into a list of rendered react elements.
          */
          var reactElements = [];

          if (!elements.length) elements = [elements];
          elements.forEach((element) => {
            if (element.nodeName != "#text") {
              reactElements.push(ReactTools.createWrappedElement(element));
            }
          });

          return reactElements;
        }

        setRemainingLocale(locale, force=false) {
          /*
            Sets the remainig localisation strings for the plugin that are not
            supported automatically by zlib.
            This includes updating the description, group names,
            and dropdown labels.
            Involves digging access to the this._config property of the Plugin class
          */
          if (this.currentLocale === locale && !force) return;
          this.currentLocale = locale;

          let strings = this._config.strings;
          if (!strings.hasOwnProperty(locale)) locale = "en";
          let settings = strings[locale].settings;
          let defaultConfig = this._config.defaultConfig;

          // Set description
          this._config.info.description = strings[locale].description;

          // Set names for groups and labels for dropdowns
          for (let i = 0; i < defaultConfig.length; i++) {
            let defaultSetting = defaultConfig[i];

            if (defaultSetting.type === DROPDOWN) {
              let defaultOptions = defaultSetting.options;
              for (let j = 0; j < defaultOptions.length; j++) {
                this._config.defaultConfig[i].options[j].label =
                  settings[defaultSetting.id].options[defaultOptions[j].value];
              }
            } else if (defaultSetting.type === CATEGORY) {
              this._config.defaultConfig[i].name = settings[defaultSetting.id].name;
              let categorySettings = defaultSetting.settings;
              for (let k = 0; k < categorySettings.length; k++) {
                let categorySetting = categorySettings[k];
                if (categorySetting.type === DROPDOWN) {
                  let defaultOptions = categorySetting.options;
                  for (let j = 0; j < defaultOptions.length; j++) {
                    this._config.defaultConfig[i].settings[k].options[j].label =
                      settings[defaultSetting.id][categorySetting.id].options[defaultOptions[j].value];
                  }
                }
              }
            }
          }
        }
    };
};
